import re
import json
import multiprocessing
import pandas as pd
import requests
import traceback

# csv_file_path="./for_mainbot_accuracy2.csv"
# out_path="./for_mainbot_accuracy_res2.csv"
csv_file_path="./Answers_Test_2_updated_-_Sheet1.csv"
out_path="./regression_oct_10_1.csv"
#process_query_url = "https://eriacc-ca.internal.ericsson.com/answers_app/process_query"
process_query_url = "https://myerica.internal.ericsson.com/answers_app/process_query"
kp_ids = [6]
#document_groups = [3,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25]
document_groups=[45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62]
#api_server = "https://eriacc-ca.internal.ericsson.com"
api_server = "https://myerica.internal.ericsson.com/"
df = pd.read_csv(csv_file_path)

def get_response(query):
    payload = {
        "kp_id": kp_ids,
        "document_group_ids": document_groups,
        "query_id": "9fbe7f7d-f2ee-44d9-85b9-f48be83c6139",
        "query_insights": {
            "user": {
                "layer_id": "dashboard_admin_test_user_1",
                "id": 8,
                "custom_properties": {}
            },
            "custom_properties": {},
            "detected_language": "en"
        },
        "query": query,
        "options": api_server,
        "conversation_id": "cd168da3d9c8c4ab51f5a2284268d004",
        "configuration": {
            "response_type": "carousel",
            "keep_html_format": True,
            "collect_feedback": False,
            "force_response": False,
            "acronym_disambiguation": False,
            "disambiguation": False,
            "acronym_intro_message": False,
            "use_keyword_filter": False,
            "strict_keyword_filter": True,
            "disambiguation_message": "Please select an option below or narrow your choices by trying a different input.",
            "view_more": True,
            "kg_answering": False,
            "answer": True,
            "use_document_embeddings": False,
            "answer_from_document_embeddings": True,
            "document_embeddings_threshold": 0.75,
            "training_data_embeddings_threshold": 0.85,
            "filtering_method": "bm25f",
            "filtering_cutoff_score": 0.0,
            "ranking_cutoff_ratio": 0.5,
            "boost_header_ngram": True,
            "dedup_response": False,
            "trim_response": True,
            "detect_true_intent": True,
            "query_token_limit": 50
        }
        }
    response = requests.post(process_query_url, data=json.dumps(payload), headers={"Content-Type": "application/json"},verify=False)
    response.raise_for_status()
    return response.json()


def get_result(query, header):
    row = {
        "Query": query,
        "Header": header
    }
    query = row["Query"]
    print(f"Sending request for -- {query}")

    try:
        response = get_response(query)

        if "carousel" in response:
            row["Cards Count"] = len(response["carousel"]["cards"])
            row["Response"] = json.dumps(response)
            row["Received Response"] = response["carousel"]["cards"][0]["card"]["section_headers"][-1]["text"]
            row["Received URL"] = response["carousel"]["cards"][0]["card"]["links"][0]["url"]

            and_flag = False
            for i, card in enumerate(response["carousel"]["cards"]):
                card = card["card"]
                card_header = card["section_headers"][-1]["text"]

                card_header = re.sub(r'\W+', '', card_header).lower().strip()
                row_header = re.sub(r'\W+', '', row["Header"]).lower().strip()

                if card_header in row_header:
                    row["Status"] = True
                    row["Rank"] = i
                    row["hybrid_score"] = card["hybrid_score"]
                    row["Received Response"] = card["section_headers"][-1]["text"]
                    row["Received URL"] = card["links"][0]["url"]

                    print(f"--> {row['Status']} -- {row['Rank']}")
                    break
                else:
                    # accordian_headers = []

                    if "&&" in row_header:
                        headers = row_header.split("&&")
                        headers = [h.strip() for h in headers]

                        if card_header in headers:
                            if and_flag:
                                row["Status"] = True
                                print(f"--> {row['Status']} -- {row['Rank']}")
                                row["hybrid_score"] = card["hybrid_score"]
                                break
                            else:
                                row["Rank"] = i
                                and_flag = True
                                row["Status"] = False

                    elif "||" in row_header:
                        headers = row_header.split("|")
                        headers = [h.strip() for h in headers]

                        if card_header in headers:
                            row["Status"] = True
                            row["Rank"] = i
                            row["hybrid_score"] = card["hybrid_score"]
                            print(f"--> {row['Status']} -- {row['Rank']}")
                            break

                    else:
                        row["Status"] = False

            if not row["Status"]:
                print(f"--> {row['Query']} ---- {row['Status']} -- Expected - {row['Header']} -- Actual - {row['Received Response']}")

        else:
            row["Status"] = False
    except:
        print(traceback.format_exc())
        row["Status"] = False

    return row


pool = multiprocessing.Pool(6)
arguments_list = []
for (idx, row) in df.iterrows():
    arguments_list.append((row["Query"], row["Header"]))
#arguments_list = arguments_list[:1]
all_rows = pool.starmap(get_result, arguments_list, chunksize=25)

new_df = pd.DataFrame()
new_df = new_df.from_records(all_rows)

for i in range(5):
    positive = len(new_df[new_df["Rank"]<=i])
    total = len(new_df)
    print(f"TOP-{i+1}: {positive/total} ({positive}/{total})")

new_df.to_csv(out_path, index=False)
